package com.ingkw.springbootvue;

import com.ingkw.springbootvue.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class VueApplicationTests {

    @Test
    void contextLoads() {
    }
    @Test
    void we() {
        int i=5;
        i+=i-=i*=i++;
        System.out.println(i);
    }

    @Test
    public void aVoid() {
    //使用orika复制工具将A复制到B对象中
        User user = new User();
        user.setId(1);
        user.setUsername("ww");
        user.setAddress("wasas");
        user.setPassword("1232");
        user.setAge(12);
        user.setNickName("www");
        user.setSex("as");
    }
}
