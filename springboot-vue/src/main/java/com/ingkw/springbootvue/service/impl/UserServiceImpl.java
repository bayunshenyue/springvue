package com.ingkw.springbootvue.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ingkw.springbootvue.model.User;
import com.ingkw.springbootvue.service.UserService;
import com.ingkw.springbootvue.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
 *
 * @author INGKW
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{
}




