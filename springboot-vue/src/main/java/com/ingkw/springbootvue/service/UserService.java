package com.ingkw.springbootvue.service;

import com.ingkw.springbootvue.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * @author INGKW
 */
public interface UserService extends IService<User> {

}
