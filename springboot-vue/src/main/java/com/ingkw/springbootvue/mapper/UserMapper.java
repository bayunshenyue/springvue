package com.ingkw.springbootvue.mapper;

import com.ingkw.springbootvue.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author INGKW
 */
public interface UserMapper extends BaseMapper<User> {

}




