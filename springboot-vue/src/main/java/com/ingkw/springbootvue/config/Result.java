package com.ingkw.springbootvue.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 说明:返回消息
 *
 * @author INGKW
 * @version 1.0
 * 时间: 2021-10-19 15:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
    /** 操作数据编码 */
    private Integer code;
    /** 消息 */
    private String msg;
    /** 操作数据结果 */
    private T data;
    /** 数据长度 */
    private Integer count;

    public static Result<Object> success(String message) {
        return new Result<>(0, message, null, null);
    }

    public static Result<Object> success(String message, Object data) {
        return new Result<>(0, message, data, null);
    }

    public static Result<Object> success(String message, Object data, int count) {
        return new Result<>(0, message, data, count);
    }

    public static Result<Object> fail() {
        return new Result<>(-1, "fail", null, null);
    }

    public static Result<Object> fail(String message) {
        return new Result<>(-1, message, null, null);
    }
}
