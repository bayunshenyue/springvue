package com.ingkw.springbootvue.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingkw.springbootvue.config.Result;
import com.ingkw.springbootvue.model.User;
import com.ingkw.springbootvue.model.vo.UserVo;
import com.ingkw.springbootvue.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 说明:
 *
 * @author INGKW
 * @version 1.0
 * 时间: 2021-11-06 17:07
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 添加
     *
     * @param user 用户
     * @return {@code Result<Object>}
     */
    @PostMapping("")
    public Result<Object> save(@RequestBody User user) {
        if (user.getPassword() == null) {
            user.setPassword("123456");
        }
        boolean save = userService.save(user);
        if (save) {
            return Result.success("添加成功");
        }
        return Result.fail("添加失败");
    }

    @GetMapping("")
    public Result<Object> findPage(@RequestParam(defaultValue = "1") Integer pageNum,
                                   @RequestParam(defaultValue = "10") Integer pageSize,
                                   @RequestParam(defaultValue = "") String search) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        Page<User> userPage = userService.page(new Page<>(pageNum, pageSize),
                Wrappers.<User>lambdaQuery().like(!StrUtil.isBlank(search), User::getNickName, search));
        return Result.success("查询成功", userPage.getRecords(), Math.toIntExact(userPage.getTotal()));
    }

    @PutMapping()
    public Result<Object> edit(@RequestBody User user) {
        boolean update = userService.updateById(user);
        if (update) {
            return Result.success("修改成功");
        }
        return Result.fail("修改失败");
    }

    @DeleteMapping()
    public Result<Object> delete(@RequestBody List<Integer> delarr) {
        boolean b = userService.removeByIds(delarr);
        if (b) {
            return Result.success("删除成功");
        }
        return Result.fail("删除失败");
    }

    @PostMapping("/login")
    public Result<Object> login(@RequestBody UserVo user) {
        System.out.println(user);
        User one =
                userService.getOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, user.getUsername()).eq(User::getPassword
                , user.getPassword()));
        if (one!=null) {
            return Result.success("登录成功");
        }
        return Result.fail("登录失败,用户名或者密码错误");
    }
}
