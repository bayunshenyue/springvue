package com.ingkw.springbootvue;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author INGKW
 */
@SpringBootApplication
@MapperScan("com.ingkw.springbootvue.mapper")
public class VueApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(VueApplication.class);
        app.run(args);
        app.setBannerMode(Banner.Mode.OFF);
    }

}
