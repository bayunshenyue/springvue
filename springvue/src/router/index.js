import { createRouter, createWebHistory } from "vue-router";
import Layout from "../Layout/Layout.vue";

const Router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "Layout",
      component: Layout,
      children: [
        {
          path: "User",
          name: "User",
          component: () => import("@/views/User.vue"),
        },
      ],
    },
    {
      path: "/Login",
      name: "Login",
      component: () => import("@/views/Login.vue"),
    },
    {
      path: "/Register",
      name: "Register",
      component: () => import("@/views/Register.vue"),
    },
  ],
});

export default Router;
