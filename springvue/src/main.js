import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// 引入css
import "./assets/css/global.css";

// 引入字体
import "./assets/icon/iconfont.css";

// ElementPlus组件
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import zhCn from "element-plus/es/locale/lang/zh-cn";

createApp(App)
  .use(ElementPlus, {
    locale: zhCn,
  })
  .use(store)
  .use(router)
  .mount("#app");
