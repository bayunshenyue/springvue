import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: [
      //find是别名，replacement是要替换的路径
      { find: "@", replacement: "/src" },
    ],
  },
  server: {
    port: 8080, //启动端口
    open: true,
    proxy: {
      "^/api": {
        target: "http://localhost:8888", // 后端服务实际地址
        changeOrigin: true, //开启代理
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
});
