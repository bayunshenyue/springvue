/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : springboot-vue

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 08/11/2021 23:34:59
*/

CREATE DATABASE springboot-vue;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `sex` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '大撒大撒', '1', '打赏', 12, '女', '大撒大撒');
INSERT INTO `user` VALUES (2, '完全', '123456', '七万五千', 112, '男', '211');
INSERT INTO `user` VALUES (3, '大撒大撒', '123456', '大撒大撒', 123, '男', '大撒大撒');
INSERT INTO `user` VALUES (4, '大撒大撒', '123456', '打赏', 12, '男', '大厦');
INSERT INTO `user` VALUES (5, '请问', '123456', '恶趣味', 1221, 'TA', '恶趣味');
INSERT INTO `user` VALUES (6, '打', '123456', '大苏打', 312, '男', '大苏打实打实');
INSERT INTO `user` VALUES (7, '大撒大撒', '123456', '啊实打实的', 2131, '女', '213312');
INSERT INTO `user` VALUES (8, 'admin', 'admin', 'admin', 12, 'TA', 'DASDAS');

SET FOREIGN_KEY_CHECKS = 1;
